var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

exports.GetAll = function(callback) {
    connection.query('SELECT * FROM Stock_User;',
        function (err, result) {
            if(err) {
                console.log(err);
                callback(true);
                return;
            }
            console.log(result);
            callback(false, result);
        }
    );
};

exports.GetByID = function(user_id, callback) {
    console.log(user_id);
    var query = 'SELECT * FROM user_info WHERE User_ID=' + user_id;
    console.log(query);
    connection.query(query,
        function (err, result) {
            if (err) {
                console.log(err);
                callback(true);
                return;
            }
            callback(false, result);
        }
    );
};

exports.Insert = function(account_info, callback) {
    console.log(account_info);
    var dynamic_query = 'INSERT INTO Stock_User (first_name, last_name, email, password) VALUES (' +
        '\'' + account_info.firstname + '\', ' +
        '\'' + account_info.lastname + '\', ' +
        '\'' + account_info.email + '\', ' +
        '\'' + account_info.password + '\' ' +
        ');';

    console.log(dynamic_query);
    connection.query(dynamic_query,

        function (err, result) {
            if(err) {
                console.log(err);
                callback(true);
                return;
            }
            callback(false, result);
        }
    );
};

var Delete = function(user_id, callback) {
    var qry = 'DELETE FROM Stock_User WHERE User_ID = ?';
    connection.query(qry, [user_id],
        function (err) {
            callback(err);
        });
};

exports.Update = function(user_id, first_name, last_name, email, password, callback) {
    console.log(user_id, first_name, last_name, email, password);
    var values = [first_name, last_name, email, password, user_id];
    connection.query('UPDATE Stock_User SET first_name = ?, last_name = ?, email = ?, password = ? WHERE User_ID = ?', values,
        function(err, result){
            if(err) {
                console.log(this.sql);
                callback(err, null);
            }
            else {
                callback(false);
            }
        });
};

exports.GetByEmail = function(email, password, callback) {
    var query = 'CALL User_GetByEmail(?, ?)';
    var query_data = [email, password];

    connection.query(query, query_data, function(err, result) {
        if(err){
            callback(err, null);
        }
        else if(result[0].length == 1) {
            callback(err, result[0][0]);
        }
        else {
            callback(err, null);
        }
    });
};

exports.DeleteById = Delete;