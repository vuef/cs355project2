var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

exports.GetAll = function(sortRate,callback) {
    connection.query('SELECT * FROM avg_rating_stock HAVING avg_rating > ?;',[sortRate],
        function (err, result) {
            if(err) {
                console.log(err);
                callback(true);
                return;
            }
            console.log(result);
            callback(false, result);
        }
    );
};

exports.Insert = function(user_id, stock_id, rating, callback) {
    var values = [rating, user_id, stock_id];
    connection.query('INSERT INTO Rate (rating, user_id, stock_id) VALUES (?, ?, ?)', values,
        function (err, result) {
            callback(err, result);
        });
};

var Delete = function(stock_id, user_id, callback) {
    var qry = 'DELETE FROM Rate WHERE stock_id = ? AND user_id = ?';
    connection.query(qry, [stock_id, user_id],
        function (err) {
            callback(err);
        });
};
exports.DeleteById = Delete;

exports.GetByID = function(stock_id, user_id, callback) {
    console.log(stock_id, user_id);
    var query = 'SELECT * FROM Rate WHERE user_id = ? AND stock_id = ?';
    console.log(query);
    connection.query(query, [stock_id, user_id],
        function (err, result) {
            if (err) {
                console.log(err);
                callback(true);
                return;
            }
            callback(false, result);
        }
    );
};