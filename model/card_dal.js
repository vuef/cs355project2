var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

exports.GetUserCard = function(user_id, callback) {
    connection.query('SELECT * FROM User_Credit_Card WHERE User_ID = ' + user_id +';',
        function (err, result) {
            if(err) {
                console.log(err);
                callback(true);
                return;
            }
            console.log(result);
            callback(false, result);
        }
    );
};

exports.GetSingleCard = function(user_id, card_number, callback) {
    var query = 'SELECT * FROM User_Credit_Card WHERE User_ID = ? AND card_number = (?);'
    connection.query(query,[user_id, card_number],
        function (err, result) {
            if(err) {
                console.log(err);
                callback(true);
                return;
            }
            console.log(result);
            callback(false, result);
        }
    );
};

exports.Insert = function(card_number, exp_date, user_id, callback) {
    var values = [card_number, exp_date, user_id];
    connection.query('INSERT INTO User_Credit_Card (card_number, expiration_date, user_id) VALUES (?, ?, ?)', values,
        function (err, result) {
            callback(err, result);
        });
};

exports.Update = function(user_id, card_number, exp_date, callback) {
    console.log(user_id, card_number,exp_date);
    var values = [card_number, exp_date, user_id];
    connection.query('UPDATE User_Credit_Card SET card_number = ?, expiration_date = ? WHERE user_id = ?', values,
        function(err, result){
            if(err) {
                console.log(this.sql);
                callback(err, null);
            }
            else {
                callback(false);
            }
        });
};

exports.ChargeUser = function(user_id, callback){
    connection.query('CALL ChargeUser('+user_id+');',
        function(err, result){
            if (err){
                console.log(err);
                callback(err, true);
            }
            else{
                callback(false);
            }
        });
};

var Delete = function(card_number, user_id, callback) {
    var qry = 'DELETE FROM User_Credit_Card WHERE card_number = ? AND user_id = ?';
    connection.query(qry, [card_number, user_id],
        function (err) {
            callback(err);
        });
};
exports.DeleteById = Delete;
