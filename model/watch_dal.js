var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

exports.GetByID = function(user_id, callback) {
    console.log(user_id);
    var query = 'SELECT * FROM watch_list WHERE User_ID=' + user_id;
    console.log(query);
    connection.query(query,
        function (err, result) {
            if (err) {
                console.log(err);
                callback(true);
                return;
            }
            else {
                callback(false, result);
            }
        }
    );
};

exports.Insert = function(stock_id, user_id, callback) {
    var dynamic_query = 'INSERT INTO Watch_List (stock_id, user_id) VALUES (' +
        '\'' + stock_id + '\', ' +
        '\'' + user_id + '\'' +
        ');';

    console.log(dynamic_query);
    connection.query(dynamic_query,

        function (err, result) {
            if(err) {
                console.log(err);
                callback(true);
                return;
            }
            callback(false, result);
        }
    );
};

var Delete = function(stock_id, user_id, callback) {
    var qry = 'DELETE FROM Watch_List WHERE stock_id = ? AND user_id = ?';
    connection.query(qry, [stock_id, user_id],
        function (err) {
            callback(err);
        });
};

exports.DeleteById = Delete;