var mysql   = require('mysql');
var db  = require('./db_connection.js');

var connection = mysql.createConnection(db.config);

exports.GetAll = function(callback) {
    connection.query('SELECT * FROM Stock;',
        function (err, result) {
            if(err) {
                console.log(err);
                callback(true);
                return;
            }
            console.log(result);
            callback(false, result);
        }
    );
};

exports.GetByID = function(stock_id, callback) {
    console.log(stock_id);
    var query = 'SELECT * FROM Stock WHERE Stock_ID =' + stock_id;
    console.log(query);
    connection.query(query,
        function (err, result) {
            if (err) {
                console.log(err);
                callback(true);
                return;
            }
            callback(false, result);
        }
    );
};


exports.Insert = function(symbol, company, price, callback) {
    var values = [symbol, company, price];
    connection.query('INSERT INTO Stock (symbol, company, price_per_stock) VALUES (?, ?, ?)', values,
        function (err, result) {
            if(err)
            {
                log(err);
                callback(true);
            }
            else
            {
                callback(false);
            }
        });
};

exports.Update = function(stock_id, symbol, company, price, callback) {
    console.log(stock_id, symbol, company, price);
    var values = [symbol, company, price, stock_id];
    connection.query('UPDATE Stock SET symbol = ?, company = ?, price_per_stock = ? WHERE Stock_ID = ?', values,
        function(err, result){
            if(err) {
                console.log(this.sql);
                callback(err, null);
            }
            else {
                callback(false);
            }
        });
};

var Delete = function(stock_id, callback) {
//function Delete(movie_id, callback) {
    var qry = 'DELETE FROM Stock WHERE Stock_ID = (?)';
    connection.query(qry,[stock_id],
        function (err) {
            console.log(this.sql);
            callback(err);
        });
};

exports.DeleteById = Delete;