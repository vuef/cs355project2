var rate = function() {

    var payload = {
        sortRate: $('#sortRate').val(),
    };

    // Next we configure the jQuery ajax call
    $.ajax({
        url: '/rate/all',  // url where we want to send the form data
        type: 'GET', // the type of form submission; GET or POST
        contentType: "json",  // the type of data we are sending
        data: payload,  // the actual data we are sending
        complete: function(data) {  // what to do with the response back from the server
            window.location.assign(this.url);
        }
    })
};


// $(document).ready() tells the browser not to run the following code until after all the HTML has been parsed
$(document).ready(function() {

    // Now that the HTML has been parsed the browser knows that there is a button with the id addBtn.
    // We are overriding the click function of the button, to run the code we specify.
    $('#sort').click(function(e) {
        // When login is clicked this console log statement logs to your browser's console log not Node.js in Webstorm
        console.log('Sort clicked');

        // this prevents the form from being submitted using the non-ajax method
        e.preventDefault();

        // runs the ajax function defined above.
        rate();
    });
});
