var express = require('express');
var router = express.Router();
var stockDal = require('../model/stock_dal');

router.get('/all', function(req, res) {
        stockDal.GetAll(function (err, result) {
            if (err) throw err;
            if(req.session.account) {
                res.render('stock/displayAllStocks.ejs',
                    {money:req.session.account.money,
                        first_name:req.session.account.first_name,rs: result,
                        user:req.session.account});
            }
            else{
                res.render('stock/displayAllStocks.ejs', {rs:result});
            }
        });
});


router.get('/', function (req, res) {
    stockDal.GetByID(req.query.stock_id, function (err, result) {
        if (err) {
            res.send("Error: " + err);
        }
        if(req.session.account) {
            res.render('stock/displayStockInfo.ejs',
                {money:req.session.account.money,
                    first_name:req.session.account.first_name,rs: result,
                    user:req.session.account});
        }
        else{
            res.render('stock/displayStockInfo.ejs',
                {rs: result});
        }
        });
});

router.get('/create', function(req,res){
    res.render('stock/stockInsertForm.ejs');
});

router.post('/insert_stock', function(req,res){
    console.log(req.body);
    stockDal.Insert(req.body.symbol, req.body.company, req.body.price,
        function(err){
            if (err) {
                res.send('Fail!<br />' + err);
            }
            else {
                res.send("Successfully saved the stock.");
            }
        });
});

router.get('/edit', function(req, res){
    console.log('/edit stock_id:' + req.query.stock_id);
    stockDal.GetByID(req.query.stock_id, function(err, stock_result){
        if(err) {
            console.log(err);
            res.send('error: ' + err);
        }
        else {
            console.log(stock_result);
            if (req.session.account) {
                res.render('stock/stockEditForm', {
                    money: req.session.account.money,
                    first_name: req.session.account.first_name, rs: stock_result
                });
            }
            else{
                res.render('stock/stockEditForm', {rs: stock_result});
            }
        }
    });
});

router.post('/update_stock', function(req,res){
    console.log(req.body);
    stockDal.Update(req.body.stock_id, req.body.symbol, req.body.company, req.body.price,
        function(err, result){
            var message;
            if(err) {
                console.log(err);
                message = 'error: ' + err;
            }
            else {
                message = 'success';
            }
            stockDal.GetByID(req.body.stock_id, function(err, stock_info){
                res.redirect('/stock/edit?stock_id=' + req.body.stock_id + '&message=' + message);
            });
        });
});

router.get('/delete', function(req, res){
    console.log(req.query);
    stockDal.GetByID(req.query.stock_id, function(err, result) {
        if(err){
            res.send("Error: " + err);
        }
        else if(result.length != 0) {
            stockDal.DeleteById(req.query.stock_id, function (err) {
                if(err){
                    res.send("Error: " + err)
                }
                else{
                    res.send(result[0].Stock_ID + ' Successfully Deleted');
                }
            });
        }
        else {
            res.send('Stock does not exist in the database.');
        }
    });
});

module.exports = router;