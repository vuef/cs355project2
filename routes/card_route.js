var express = require('express');
var router = express.Router();
var cardDal = require('../model/card_dal');

router.get('/', function (req, res) {
    if(req.session.account) {
        cardDal.GetUserCard(req.session.account.User_ID, function (err, result) {
                if (err){
                    throw err;
                }
                else
                {
                    res.render('card/viewCards.ejs',
                        {money:req.session.account.money,
                            first_name:req.session.account.first_name,rs:result});
                }
            });
        }
    else{
        res.render('authentication/login.ejs');
    }
});

router.get('/create', function(req,res){
    if(req.session.account) {
        res.render('card/cardInsertForm.ejs',
            {money:req.session.account.money,
                first_name:req.session.account.first_name, user:req.session.account.User_ID});
    }
    else
    {
        res.render('authentication/login.ejs');
    }
});

router.post('/insert_card', function(req,res){
    if(req.session.account) {
        console.log(req.body);
        cardDal.Insert(req.body.card_number, req.body.exp_date, req.session.account.User_ID,
            function (err) {
                if (err) {
                    res.send('Fail!<br />' + err);
                }
                else {
                    res.send("Successfully saved the card.");
                }
            });
    }
    else
        {
            res.render('authentication/login.ejs');
        }
    });

router.get('/edit', function(req, res){
    if(req.session.account) {
        console.log('/edit user_card:' + req.session.account.User_ID);
        cardDal.GetSingleCard(req.session.account.User_ID, req.query.card_number, function (err, card_result) {
            if (err) {
                console.log(err);
                res.send('error: ' + err);
            }
            else {
                console.log(card_result);
                res.render('card/cardEditForm.ejs',
                    {money:req.session.account.money,
                        first_name:req.session.account.first_name,rs: card_result});
            }
        });
    }
    else
        {
            res.render('authentication/login.ejs');
        }
});

router.post('/update_card', function(req,res){
    if(req.session.account) {
        console.log(req.body);
        cardDal.Update(req.session.account.User_ID, req.body.card_number, req.body.exp_date,
            function (err, result) {
                var message;
                if (err) {
                    console.log(err);
                    message = 'error: ' + err;
                }
                else {
                    message = 'success';
                }
                cardDal.GetSingleCard(req.session.account.User_ID,req.body.card_number, function (err, card_info) {
                    res.redirect('/card/edit?user_id=' + req.session.account.User_ID
                        + '&card_number=' + req.body.card_number + '&message=' + message);
                });
            });
    }
    else
    {
        res.render('authentication/login.ejs');
    }
});

router.get('/delete', function(req, res){
    console.log(req.query);
    if(req.session.account) {
        cardDal.GetSingleCard(req.session.account.User_ID, req.query.card_number, function (err, result) {
            if (err) {
                res.send("Error: " + err);
            }
            else if (result.length != 0) {
                cardDal.DeleteById(req.query.card_number, req.session.account.User_ID, function (err){
                    if (err) {
                        res.send("Error: " + err);
                    }
                    else
                    {
                        res.send(result[0].card_number + ' Successfully Deleted');
                    }
                });
            }
            else {
                res.send('No cards were deleted.');
            }
        });
    }
    else
    {
        res.render('authentication/login.ejs');
    }
});

module.exports = router;