var express = require('express');
var router = express.Router();
var buyDal = require('../model/buy_dal');
var cardDal = require('../model/card_dal');

router.get('/insert', function (req, res) {
    buyDal.Insert(req.query.stock_id, req.query.user_id,
        function (err, result) {
            var response = {};
            if(err) {
                response.message = err.message;
            }
            else {
                cardDal.ChargeUser(req.query.user_id,
                    function(err){
                    if(err){
                        res.send(err);
                    }
                    else {
                        res.send("Successfully bought the stock.");
                    }
                })
            }
        });
});

router.get('/delete', function(req, res){
    console.log(req.query);
    if(req.session.account) {
        buyDal.GetByID(req.session.account.User_ID, function (err, result) {
            if (err) {
                res.send("Error: " + err);
            }
            else if (result.length != 0) {
                buyDal.DeleteById(req.query.stock_id, req.query.user_id, function (err){
                    if (err) {
                        res.send("Error: " + err);
                    }
                    else
                    {
                        cardDal.ChargeUser(req.query.user_id, function(err){
                            res.send(result[0].Stock_ID + ' Successfully Sold');
                        });
                    }
                });
            }
            else {
                res.send('No stocks were sold.');
            }
        });
    }
    else
    {
        res.render('authentication/login.ejs');
    }
});

module.exports = router;