var express = require('express');
var router = express.Router();
var userDal = require('../model/user_dal');
var buyDal = require('../model/buy_dal');

router.get('/', function(req, res) {
    if(req.session.account){
        userDal.GetByID(req.session.account.User_ID, function (err, result) {
                if (err){
                    return(true);
                }
                else {
                    buyDal.GetByID(req.session.account.User_ID, function(err, buy_result)
                    {
                       if (err) {
                           return(true);
                       }
                        else{
                           res.render('user/displayUserInfo.ejs',
                               {money:req.session.account.money,
                                   first_name: req.session.account.first_name,
                                   rs: result, bs: buy_result});
                       }
                    });
                }
            }
        );
    }
    else {
        res.render('authentication/login.ejs');
    }
});

router.get('/all', function(req, res) {
    userDal.GetAll(function (err, result) {
        if (err) throw err;
        if (req.session.account) {
            res.render('user/displayAllUsers.ejs', {
                money: req.session.account.money,
                first_name: req.session.account.first_name, rs: result
            });
        }
        else {
            res.render('user/displayAllUsers.ejs',{rs:result});
        }
    });
});

router.get('/create',function(req,res) {
        res.render('user/userFormCreate.ejs');
});

router.get('/save', function(req, res, next) {
    userDal.Insert(req.query, function(err, result){
        if (err) {
            res.send(err);
        }
        else {
            res.send("Successfully saved the user.");
        }
    });
});

router.get('/edit', function(req, res){
    console.log('/edit user_id:' + req.query.user_id);
    userDal.GetByID(req.query.user_id, function(err, user_result){
        if(err) {
            console.log(err);
            res.send('error: ' + err);
        }
        else {
            console.log(user_result);
            res.render('user/user_edit_form', {rs: user_result});
        }
    });
});

router.post('/update_user', function(req,res){
    console.log(req.body);
    userDal.Update(req.body.user_id, req.body.first_name, req.body.last_name, req.body.email, req.body.password,
        function(err, result){
            var message;
            if(err) {
                console.log(err);
                message = 'error: ' + err;
            }
            else {
                message = 'success';
            }
            userDal.GetByID(req.body.user_id, function(err, user_info){
                console.log(user_info);
                res.redirect('/user/edit?user_id=' + req.body.user_id + '&message=' + message);
            });
        });
});

router.get('/delete', function(req, res) {
    userDal.GetByID(req.query.user_id, function(err, result){
        if(err){
            res.send("Error: " + err);
        }
        else if(result.length != 0) {
            userDal.DeleteById(req.query.user_id, function (err) {
                if (err) {
                    res.send("Error: " + err);
                }
                else {
                    res.send(result[0].User_ID + ' Successfully Deleted');
                }
            });
        }
        else {
            res.send('User does not exist in the database.');
        }
    });
});

module.exports = router;