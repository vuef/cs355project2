var express = require('express');
var router = express.Router();
var userDal = require('../model/user_dal');

/* GET home page. */
router.get('/', function(req, res, next) {
  var data = {
    title : 'Express'
  }
  //res.render('index', { title: 'CS355', subtitle: 'Lab 8' });
  if(req.session.account === undefined) {
    res.render('index', data);
  }
  else {
    data.first_name = req.session.account.first_name;
    data.money = req.session.account.money;
    res.render('index', data);
  }
});

router.get('/about', function(req, res, next) {
  res.render('about');
});

router.get('/authenticate', function(req, res) {
  userDal.GetByEmail(req.query.email, req.query.password, function (err, account) {
    if (err) {
      res.send(err);
    }
    else if (account == null) {
      res.send("User not found.");
    }
    else {
      req.session.account = account;
      console.log(account);
      if(req.session.originalUrl = '/login') {
        req.session.originalUrl = '/'; //don't send user back to login, instead forward them to the homepage.
      }
      res.redirect(req.session.originalUrl);
    }
  });
});

router.get('/login', function(req, res, next) {
  if(req.session.account) {
    res.render('/');
  }
  else {
    res.render('authentication/login.ejs');
  }

});

router.get('/logout', function(req, res) {
  req.session.destroy( function(err) {
    res.render('authentication/logout.ejs');
  });
});

module.exports = router;