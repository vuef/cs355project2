var express = require('express');
var router = express.Router();
var rateDal = require('../model/rate_dal');

router.get('/all', function (req, res) {
        rateDal.GetAll(req.query.sortRate,function (err, result) {
            if (err) throw err;
            if(req.session.account) {
                res.render('rate/displayAvgRating.ejs',
                    {money:req.session.account.money,
                        first_name: req.session.account.first_name, rs: result,
                        sort:req.query.sortRate});
            }
            else {
                res.render('rate/displayAvgRating.ejs', {rs: result, sort:req.query.sortRate});
            }
        });
});

router.get('/create', function(req, res){
    if(req.session.account) {
        rateDal.GetAll(0,function(err, result){
            if(err) {
                res.send("Error: " + err);
            }
            else {
                console.log(req);
                res.render('rate/rateInsertForm.ejs',
                    {money:req.session.account.money,
                        first_name:req.session.account.first_name,stocks: result,
                        user:req.session.account, getParameterByName:
                        function (name) {
                        var url = req.originalUrl;
                        name = name.replace(/[\[\]]/g, "\\$&");
                        var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
                            results = regex.exec(url);
                        if (!results) return null;
                        if (!results[2]) return '';
                        return decodeURIComponent(results[2].replace(/\+/g, " "));
                        }
                    });
            }
        });
    }
    else{
        res.render('authentication/login.ejs');
    }
});

router.get('/insert', function (req, res) {
    rateDal.Insert(req.query.user_id, req.query.stock_id, req.query.rating,
        function (err, result) {
            var response = {};
            if(err) {
                response.message = err.message;
            }
            else {
                response.message = 'Success!';
            }
            res.json(response);
        });
});

router.get('/delete', function(req, res){
    console.log(req.query);
    if(req.session.account) {
        rateDal.GetSingleCard(req.session.account.User_ID, req.query.stock_id, function (err, result) {
            if (err) {
                res.send("Error: " + err);
            }
            else if (result.length != 0) {
                rateDal.DeleteById(req.query.stock_id, req.session.account.User_ID, function (err){
                    if (err) {
                        res.send("Error: " + err);
                    }
                    else
                    {
                        res.send('Successfully Deleted');
                    }
                });
            }
            else {
                res.send('No cards were deleted.');
            }
        });
    }
    else
    {
        res.render('authentication/login.ejs');
    }
});

module.exports = router;