var express = require('express');
var router = express.Router();
var watchDal = require('../model/watch_dal');
var stockDal = require('../model/stock_dal');

router.get('/all', function (req, res) {

    if(req.session.account) {
        watchDal.GetByID(req.session.account.User_ID, function (err, result) {
            if (err) {
                res.send("Error: " + err);
            }
            else {
                stockDal.GetAll(function (err, stock_result) {
                        if (err){
                            return(true);
                        }
                    else{
                            res.render('watch/displayWatchList.ejs',
                                {money:req.session.account.money,
                                    first_name: req.session.account.first_name,
                                    rs: result, user: req.session.account, stocks: stock_result});
                        }
                    }
                );
            }
        });
    }
    else
    {
        res.render('authentication/login.ejs');
    }
});

router.get('/insert', function (req, res) {
    watchDal.Insert(req.query.stock_id, req.query.user_id,
        function (err, result) {
            var response = {};
            if(err) {
                response.message = err.message;
            }
            else {
                response.message = 'Success!';
            }
            res.json(response);
        });
});

router.get('/delete', function(req, res){
    console.log(req.query);
    if(req.session.account) {
        watchDal.GetByID(req.session.account.User_ID, function (err, result) {
            if (err) {
                res.send("Error: " + err);
            }
            else if (result.length != 0) {
                watchDal.DeleteById(req.query.stock_id, req.query.user_id, function (err){
                    if (err) {
                        res.send("Error: " + err);
                    }
                    else
                    {
                        res.send(result[0].Stock_ID + ' Successfully Deleted From Watch List');
                    }
            });
            }
            else {
                res.send('Stock does not exist in your watch list.');
            }
        });
    }
    else
    {
        res.render('authentication/login.ejs');
    }
});

module.exports = router;